import React, { useState, useEffect } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { DrawerContentScrollView, DrawerItemList } from '@react-navigation/drawer';
import { Avatar, Drawer } from 'react-native-paper';
import { useDispatch, useSelector } from 'react-redux';
import { logout } from './store/actions/auth';

const CustomDrawerContent = (props) => {
  const { navigation } = props;

  const isLoggedIn = useSelector(state => state.auth.isAuthenticated);
  
  const dispatch = useDispatch();

  const handleLogout = async () => {
    try {
      dispatch(logout());
      navigation.navigate('Login');
    } catch (error) {
      console.error('Error logging out:', error);
    }
  };

  return (
    <View style={{ flex: 1 }}>
      {/* Header section */}
      <DrawerContentScrollView {...props}>
        <Drawer.Section style={styles.drawerSection}>
          <Avatar.Image
            source={{ uri: 'https://gitlab.com/uploads/-/system/user/avatar/3400314/avatar.png?width=96' }}
            size={50}
          />
          <Text style={styles.headerText}>Welcome</Text>
        </Drawer.Section>
        <DrawerItemList {...props} />
      </DrawerContentScrollView>

      {/* Bottom section */}
      <Drawer.Section style={styles.bottomDrawerSection}>
        {isLoggedIn && (
          <Drawer.Item
            icon="logout"
            label="Logout"
            labelStyle={styles.logoutText}
            onPress={handleLogout} // Added onPress directly to the Drawer.Item
          />
        )}
      </Drawer.Section>
    </View>
  );
};

const styles = StyleSheet.create({
  drawerSection: {
    marginTop: 15,
    paddingLeft: 20,
    alignItems: 'center',
    flexDirection: 'row',
  },
  headerText: {
    marginLeft: 10,
    fontSize: 18,
    fontWeight: 'bold',
  },
  bottomDrawerSection: {
    marginBottom: 15,
    borderTopColor: '#f4f4f4',
    borderTopWidth: 1,
  },
  logoutText: {
    color: 'red',
    fontSize: 16,
  },
});

export default CustomDrawerContent;

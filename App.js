import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { createStackNavigator } from '@react-navigation/stack';

import LoginScreen from './LoginScreen';
import HomeScreen from './HomeScreen';
import NotificationsScreen from './NotificationsScreen';
import CustomDrawerContent from './CustomDrawerContent';

import authReducer from './store/reducers/auth';
import thunk from 'redux-thunk';
import { createStore, combineReducers, applyMiddleware  } from 'redux';
import { Provider } from 'react-redux';

const rootReducer = combineReducers({
  auth: authReducer,
  // TODO: Add more reducers
});

const Drawer = createDrawerNavigator();
const Stack = createStackNavigator();

const store = createStore(rootReducer, applyMiddleware(thunk));

const MainNavigator = () => {
  return (
    <Drawer.Navigator 
      useLegacyImplementation
      drawerContent={props => <CustomDrawerContent {...props} />}>
        <Drawer.Screen name="Home" component={HomeScreen} />
        <Drawer.Screen name="Notifications" component={NotificationsScreen} />
         {/* The "Login" screen is automatically hidden based on the authentication status */}
    </Drawer.Navigator>
  );
};

const LoginNavigator = () => {
  return (
    <Stack.Navigator screenOptions={{ headerShown: false }} initialRouteName="Login">
        <Stack.Screen name="Login" component={LoginScreen} />
        <Stack.Screen name="Home" component={HomeScreen} />
         {/* The "Login" screen is automatically hidden based on the authentication status */}
    </Stack.Navigator>
  );
};

const App = () => {
  return (
    <Provider store={store}>
      <NavigationContainer>
        <Stack.Navigator screenOptions={{ headerShown: false }} initialRouteName="Auth">
          <Stack.Screen name="Main" component={MainNavigator} />
          <Stack.Screen name="Auth" component={LoginNavigator} />
          <Stack.Screen name="Login" component={LoginScreen} />
        </Stack.Navigator>
      </NavigationContainer>
    </Provider>
  );
};

export default App;

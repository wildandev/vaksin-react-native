import React, { useState, useEffect } from 'react';
import { View, Text, StyleSheet, Button } from 'react-native';
import { useIsFocused } from '@react-navigation/native';
import { useDispatch, useSelector } from 'react-redux';
import { logout } from './store/actions/auth';
import { getProtectedUserData } from './store/actions/user';

const HomeScreen = ({ navigation }) => {
  const [hiddenData, setHiddenData] = useState({});

  const dispatch   = useDispatch();
  const isFocused  = useIsFocused(); // Get the focused status
  const isLoggedIn = useSelector(state => state.auth.isAuthenticated);

  useEffect(() => {
    console.log("IsLoggedIn: ", isLoggedIn); // Debug mode

    const fetchUserData = async () => {
      try {
        if (isFocused) {
          const data = await getProtectedUserData();
          setHiddenData(data);
        }
      } catch (error) {
        console.error('Error retrieving token:', error);
      }
    };

    if (!isLoggedIn) {
      // Only navigate if isLoggedIn is false and the screen is focused
      if (isFocused) {
        navigation.navigate('Login');
      }
    } else {
      fetchUserData(isFocused); // Fetch data when user is logged in
    }
  }, [isLoggedIn, isFocused]);

  const handleLogout = async () => {
    try {
      dispatch(logout());
    } catch (error) {
      console.error('Error logging out:', error);
    }
  };

  return (
    <View style={styles.container}>
      {hiddenData.name ? (
        <Text style={styles.text}>Name: {hiddenData.name}</Text>
      ) : null}
      {hiddenData.email ? (
        <Text style={styles.text}>Email: {hiddenData.email}</Text>
      ) : null}
      <Button title="Log Out" onPress={handleLogout} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  text: {
    fontSize: 18,
  },
});

export default HomeScreen;

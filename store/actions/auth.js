import axios from 'axios';
import { saveToken, removeToken } from '../../TokenUtils';

export const LOGIN_SUCCESS = 'LOGIN_SUCCESS';
export const LOGOUT = 'LOGOUT';

export const loginSuccess = () => {
  return {
    type: LOGIN_SUCCESS,
  };
};

export const logout = () => (dispatch) => {
  removeToken();
  dispatch({ type: LOGOUT });
};

export const login = (email, password) => {
  return async dispatch => {
    const response = await axios.post('http://127.0.0.1:8000/api/login', {
      email,
      password,
    });

    if (response.status === 200) {
      const token = response.data.token;
      saveToken(token);
      dispatch(loginSuccess());
    } 
  }
};

import axios from 'axios';
import { getToken } from '../../TokenUtils';

const api = axios.create({
  baseURL: 'http://127.0.0.1:8000/',
});

export const getProtectedUserData = async () => {
  const token = await getToken();
  
  if (token) {
    try {
      const response = await api.get('/api/user', {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });
      return response.data;
    } catch (error) {
      console.error('Error fetching protected data:', error);
      return null;
    }
  } else {
    console.error('No token available');
    return null;
  }
};

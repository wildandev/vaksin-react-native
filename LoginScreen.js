import React, { useState, useEffect } from 'react';
import { View, StyleSheet } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import { TextInput, Button, Text, Checkbox } from 'react-native-paper';
import { login } from './store/actions/auth';

const LoginScreen = ({ navigation }) => {
  const [email,        setEmail]        = useState('');
  const [password,     setPassword]     = useState('');
  const [showPassword, setShowPassword] = useState(false); // State for showing/hiding password
  const [errorMessage, setErrorMessage] = useState(''); // State to hold error message

  const dispatch   = useDispatch();
  const isLoggedIn = useSelector(state => state.auth.isAuthenticated);

  console.log("IsLoggedIn: ", isLoggedIn); // Debug mode

  const handleLogin = async () => {
    try {
      await dispatch(login(email, password));
      navigation.navigate('Main');
    } catch (error) {
      setErrorMessage('Invalid email or password.');
      console.error('Error logging in:', error);
    }
  };  

  return (
    <View style={styles.container}>
      <View style={styles.passwordContainer}>
        <TextInput
          label="Email"
          value={email}
          onChangeText={setEmail}
          style={styles.input}
          keyboardType="email-address"
          autoCapitalize="none"
        />
        <TextInput
          label="Password"
          value={password}
          onChangeText={setPassword}
          secureTextEntry={!showPassword} // Toggle secureTextEntry based on showPassword state
          style={styles.input}
        />
        <View style={styles.showPasswordContainer}>
          <Text>Show password</Text>
          <Checkbox
            status={showPassword ? 'checked' : 'unchecked'}
            onPress={() => setShowPassword(!showPassword)}
            color="blue"
          />
        </View>
      </View>
      <Button mode="contained" onPress={handleLogin} style={styles.button}>
        Login
      </Button>
      {errorMessage ? <Text style={styles.errorMessage}>{errorMessage}</Text> : null}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 20,
  },
  input: {
    width: '100%',
    marginBottom: 15,
  },
  passwordContainer: {
    width: '100%',
  },
  showPasswordContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  errorMessage: {
    color: 'red',
    marginTop: 5,
    fontSize: 16,
  },
  button: {
    marginTop: 15,
    width: '100%',
    justifyContent: 'center',
    paddingVertical: 10,
  },
});

export default LoginScreen;
